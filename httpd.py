# Copyright (c) 2007-2009, Mamta Singh. All rights reserved. see README for details.

'''


If an application wants to use this module as a library, it can launch the server as follows:
>>> agent = HTTPServer()   # a new RTMP server instance
>>> agent.root = 'flvs/'    # set the document root to be 'flvs' directory. Default is current './' directory.
>>> agent.start()           # start the server
>>> multitask.run()         # this is needed somewhere in the application to actually start the co-operative multitasking.


If an application wants to specify a different application other than the default App, it can subclass it and supply the application by
setting the server's apps property. The following example shows how to define "myapp" which invokes a 'connected()' method on client when
the client connects to the server.

class MyApp(App):         # a new MyApp extends the default App in rtmp module.
    def __init__(self):   # constructor just invokes base class constructor
        App.__init__(self)
    def onConnect(self, client, *args):
        result = App.onConnect(self, client, *args)   # invoke base class method first
        def invokeAdded(self, client):                # define a method to invoke 'connected("some-arg")' on Flash client
            client.call('connected', 'some-arg')
            yield
        multitask.add(invokeAdded(self, client))      # need to invoke later so that connection is established before callback
...
agent.apps = dict({'myapp': MyApp, 'someapp': MyApp, '*': App})

Now the client can connect to rtmp://server/myapp or rtmp://server/someapp and will get connected to this MyApp application.
If the client doesn't define "function connected(arg:String):void" in the NetConnection.client object then the server will
throw an exception and display the error message.

'''

import os, sys, time, struct, socket, traceback, multitask
import threading, Queue
import uuid
import select
from string import strip

from BaseHTTPServer import BaseHTTPRequestHandler
from SimpleAppHTTPServer import SimpleAppHTTPRequestHandler
from cStringIO import StringIO
from cookielib import parse_ns_headers, CookieJar
from Cookie import SimpleCookie

global _debug
_debug = False
def set_debug(dbg):
    global _debug
    _debug = dbg

class MultitaskHTTPRequestHandler(BaseHTTPRequestHandler):

    def setup(self):
        self.connection = self.request
        self.rfile = StringIO()
        self.wfile = StringIO()

    def finish(self):
        pass

    def info(self):
        return self.headers

    def get_full_url(self):
        return self.path

    def get_header(self, hdr, default):
        return self.headers.getheader(hdr, default)

    def add_cookies(self):
        for k, v in self.response_cookies.items():
            val = v.OutputString()
            self.send_header("Set-Cookie", val)


def process_cookies(headers, remote, cookie_key="Cookie", add_sess=True):
    ch = headers.getheaders(cookie_key)
    if _debug:
        print "messageReceived cookieheaders=", '; '.join(ch)
    res = []
    for c in ch:
        c = c.split(";")
        if len(c) == 0:
            continue
        c = map(strip, c)
        c = filter(lambda x: x, c)
        res += c
    has_sess = False
    response_cookies = SimpleCookie()
    for c in res:
        if _debug:
            print "found cookie", repr(c)
        name, value = c.split("=")
        response_cookies[name] = value
        #response_cookies[name]['path'] = "/"
        #response_cookies[name]['domain'] = remote[0]
        #response_cookies[name]['version'] = 0
        if name == "session":
            response_cookies[name]['expires'] = 50000
            has_sess = True
    if not add_sess:
        return response_cookies
    if not has_sess:
        response_cookies['session'] = uuid.uuid4().hex
        response_cookies['session']['expires'] = 50000
        #response_cookies['session']['path'] = '/'
        #response_cookies['session']['domain'] = remote[0]
        #response_cookies['session']['version'] = 0
    return response_cookies


class ConnectionClosed:
    'raised when the client closed the connection'


class SockStream(object):
    '''A class that represents a socket as a stream'''
    def __init__(self, sock):
        self.sock, self.buffer = sock, ''
        self.bytesWritten = self.bytesRead = 0

    def close(self):
        self.sock.shutdown(1) # can't just close it.
        self.sock = None

    def readline(self):
        try:
            while True:
                nl = self.buffer.find("\n")
                if nl >= 0: # do not have newline in buffer
                    data, self.buffer = self.buffer[:nl+1], self.buffer[nl+1:]
                    raise StopIteration(data)
                data = (yield multitask.recv(self.sock, 4096)) # read more from socket
                if not data: raise ConnectionClosed
                if _debug: print 'socket.read[%d] %r'%(len(data), data)
                self.bytesRead += len(data)
                self.buffer += data
        except StopIteration: raise
        except: raise ConnectionClosed # anything else is treated as connection closed.

    def read(self, count):
        try:
            while True:
                if len(self.buffer) >= count: # do not have data in buffer
                    data, self.buffer = self.buffer[:count], self.buffer[count:]
                    raise StopIteration(data)
                data = (yield multitask.recv(self.sock, 4096)) # read more from socket
                if not data: raise ConnectionClosed
                # if _debug: print 'socket.read[%d] %r'%(len(data), data)
                self.bytesRead += len(data)
                self.buffer += data
        except StopIteration: raise
        except: raise ConnectionClosed # anything else is treated as connection closed.

    def unread(self, data):
        self.buffer = data + self.buffer

    def write(self, data):
        while len(data) > 0: # write in 4K chunks each time
            chunk, data = data[:4096], data[4096:]
            self.bytesWritten += len(chunk)
            if _debug: print 'socket.write[%d] %r'%(len(chunk), chunk)
            try: yield multitask.send(self.sock, chunk)
            except: raise ConnectionClosed
        if _debug: print 'socket.written'


class Protocol(object):

    def __init__(self, sock):
        self.stream = SockStream(sock)
        self.writeLock = threading.Lock()
        self.writeQueue = Queue.Queue()

    def messageReceived(self, msg):
        yield

    def connectionClosed(self):
        yield

    def parse(self):
        try:
            yield self.parseRequests()   # parse http requests
        except ConnectionClosed:
            #yield self.connectionClosed()
            if _debug: print 'parse connection closed'
            #yield self.server.queue.put((self, None)) # close connection

    def removeConnection(self):
        yield self.server.queue.put((self, None)) # close connection

    def writeMessage(self, message):
        try:
            yield self.stream.write(message)
        except ConnectionClosed:
            yield self.connectionClosed()
        except:
            print traceback.print_exc()
        #self.writeQueue.put(message)

    def parseRequests(self):
        '''Parses complete messages until connection closed. Raises ConnectionLost exception.'''

        if _debug:
            print "parseRequests start", repr(self)
        self.hr = MultitaskHTTPRequestHandler(self.stream, self.remote, None)
        self.hr.close_connection = 1
        self.cookies = CookieJar()

        while True:

            # prepare reading the request so that when it's handed
            # over to "standard" HTTPRequestHandler, the data's already
            # there.
            if _debug:
                print "parseRequests"
            try:
                readok = (yield multitask.readable(self.stream.sock, 5000))
            except select.error:
                print "select error: connection closed"
                raise ConnectionClosed

            if _debug:
                print "readok", readok
                print
            raw_requestline = (yield self.stream.readline())
            if _debug: print "parseRequests, line", raw_requestline
            if not raw_requestline:
                raise ConnectionClosed
            data = ''
            try:
                while 1:
                    line = (yield self.stream.readline())
                    data += line
                    if line in ['\n', '\r\n']:
                        break
            except StopIteration:
                if _debug: print "parseRequests, stopiter"
                raise
            except:
                if _debug:
                    print 'parseRequests', \
                          (traceback and traceback.print_exc() or None)

                raise ConnectionClosed

            self.hr.raw_requestline = raw_requestline
            #pos = self.hr.rfile.tell()
            pos = 0
            self.hr.rfile.truncate(0)
            self.hr.rfile.write(data)
            if _debug:
                print "parseRequests write after"
            self.hr.rfile.seek(pos)
            if _debug:
                print "parseRequests seek after"

            if not self.hr.parse_request():
                raise ConnectionClosed
            if _debug:
                print "parseRequests parse_req after"
                print "parseRequest headers", repr(self.hr.headers), str(self.hr.headers)
            try:
                yield self.messageReceived(self.hr)
            except ConnectionClosed:
                if _debug:
                    print 'parseRequests, ConnectionClosed '
                    raise StopIteration

            except:
                if _debug:
                    print 'messageReceived', \
                          (traceback and traceback.print_exc() or None)

    def write(self):
        '''Writes messages to stream'''
        print "starting protocol write loop", repr(self)
        while True:
            while self.writeQueue.empty(): (yield multitask.sleep(0.01))
            data = self.writeQueue.get() # TODO this should be used using multitask.Queue and remove previous wait.
            if _debug: print "write to stream", repr(data)
            if data is None:
                # just in case TCP socket is not closed, close it.
                try:
                    if _debug:
                        print "stream closing"
                        print self.stream
                    yield self.stream.close()
                    if _debug:
                        print "stream closed"
                except: pass
                break

            try:
                yield self.stream.write(data)
            except ConnectionClosed:
                yield self.connectionClosed()
            except:
                print traceback.print_exc()
        print "ending protocol write loop", repr(self)


class Stream(object):
    '''The stream object that is used for RTMP stream.'''
    count = 0
    def __init__(self, client):
        self.client, self.id, self.name = client, 0, ''
        self.recordfile = self.playfile = None # so that it doesn't complain about missing attribute
        self.queue = multitask.Queue()
        self._name = 'Stream[' + str(Stream.count) + ']'; Stream.count += 1
        if _debug: print self, 'created'

    def close(self):
        if _debug: print self, 'closing'
        if self.recordfile is not None: self.recordfile.close(); self.recordfile = None
        if self.playfile is not None: self.playfile.close(); self.playfile = None
        self.client = None # to clear the reference
        pass

    def __repr__(self):
        return self._name

    def recv(self):
        '''Generator to receive new Message on this stream, or None if stream is closed.'''
        return self.queue.get()


class Client(Protocol):
    '''The client object represents a single connected client to the server.'''
    def __init__(self, sock, server, remote):
        Protocol.__init__(self, sock)
        self.server = server
        self.remote = remote
        self.streams = {}
        self.queue = multitask.Queue() # receive queue used by application
        multitask.add(self.parse())
        #multitask.add(self.write())

    def recv(self):
        '''Generator to receive new Message (msg, arg) on this stream, or (None,None) if stream is closed.'''
        return self.queue.get()

    def connectionClosed(self):
        '''Called when the client drops the connection'''
        if _debug: 'Client.connectionClosed'
        #self.writeMessage(None)
        #yield self.queue.put((None,None))
        if self.stream.sock:
            yield self.stream.close()
        else:
            yield None

    def messageReceived(self, msg):
        if _debug: print 'messageReceived cmd=', msg.command, msg.path
        msg.response_cookies = process_cookies(msg.headers, self.remote)

        # slightly bad, this: read everything, put it into memory,
        # but it helps to jump-start the project and enables use of
        # standard http://python.org BaseHTTPRequestHandler.
        # modification of mimetools.Message (actually rfc822) is
        # otherwise required.
        if msg.headers.has_key('content-length'):
            max_chunk_size = 10*1024*1024
            size_remaining = int(msg.headers["content-length"])
            L = []
            while size_remaining:
                chunk_size = min(size_remaining, max_chunk_size)
                data = (yield self.stream.read(chunk_size))
                L.append(data)
                size_remaining -= len(L[-1])

            pos = msg.rfile.tell()
            msg.rfile.write(''.join(L))
            msg.rfile.seek(pos)

        yield self.server.queue.put((self, msg)) # new connection

    def rejectConnection(self, reason=''):
        '''Method to reject an incoming client.  just close.
           TODO: report back an HTTP error with "reason" in it.
        '''
        self.removeConnection()


class Server(object):
    '''A RTMP server listens for incoming connections and informs the app.'''
    def __init__(self, sock):
        '''Create an RTMP server on the given bound TCP socket. The server will terminate
        when the socket is disconnected, or some other error occurs in listening.'''
        self.sock = sock
        self.queue = multitask.Queue()  # queue to receive incoming client connections
        multitask.add(self.run())

    def recv(self):
        '''Generator to wait for incoming client connections on this server and return
        (client, args) or (None, None) if the socket is closed or some error.'''
        return self.queue.get()

    def run(self):
        try:
            while True:
                sock, remote = (yield multitask.accept(self.sock))  # receive client TCP
                if sock == None:
                    if _debug: print 'rtmp.Server accept(sock) returned None.'
                    break
                if _debug: print 'connection received from', remote
                sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1) # make it non-block
                client = Client(sock, self, remote)
        except:
            if _debug: print 'rtmp.Server exception ', (sys and sys.exc_info() or None)
            traceback.print_exc()

        if (self.sock):
            try: self.sock.close(); self.sock = None
            except: pass
        if (self.queue):
            yield self.queue.put((None, None))
            self.queue = None

class BaseApp(object):
    '''An application instance containing any number of streams. Except for constructor all methods are generators.'''
    count = 0
    def __init__(self):
        self.name = str(self.__class__.__name__) + '[' + str(App.count) + ']'; App.count += 1
        self.players, self.publishers, self._clients = {}, {}, [] # Streams indexed by stream name, and list of clients
        if _debug: print self.name, 'created'
    def __del__(self):
        if _debug: print self.name, 'destroyed'
    @property
    def clients(self):
        '''everytime this property is accessed it returns a new list of clients connected to this instance.'''
        return self._clients[1:] if self._clients is not None else []


class App(BaseApp, SimpleAppHTTPRequestHandler):
    pass


class HTTPServer(object):
    '''A RTMP server to record and stream HTTP.'''
    def __init__(self):
        '''Construct a new HttpServer. It initializes the local members.'''
        self.sock = self.server = None
        self.apps = dict({'*': App}) # supported applications: * means any as in {'*': App}
        self.clients = dict()  # list of clients indexed by scope. First item in list is app instance.
        self.root = ''

    def start(self, host='0.0.0.0', port=8080):
        '''This should be used to start listening for RTMP connections on the given port, which defaults to 8080.'''
        if _debug: print 'start', host, port
        if not self.server:
            sock = self.sock = socket.socket(type=socket.SOCK_STREAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((host, port))
            if _debug: print 'listening on ', sock.getsockname()
            sock.listen(5)
            server = self.server = Server(sock) # start rtmp server on that socket
            multitask.add(self.serverlistener())

    def stop(self):
        if _debug: print 'stopping HTTP server'
        if self.server and self.sock:
            try: self.sock.close(); self.sock = None
            except: pass
        self.server = None

    def serverlistener(self):
        '''Server listener (generator). It accepts all connections and invokes client listener'''
        try:
            while True:  # main loop to receive new connections on the server
                client, msg = (yield self.server.recv()) # receive an incoming client connection.
                # TODO: we should reject non-localhost client connections.
                if not client:                # if the server aborted abnormally,
                    break                     #    hence close the listener.
                if _debug: print 'client connection received', client, msg

                if msg is None:
                    yield client.connectionClosed()
                    session = client.session
                    del self.clients[session]
                    continue

                if _debug:
                    print "cookies", str(msg.response_cookies)
                session = msg.response_cookies['session'].value
                client.session = session
                name = msg.path
                if _debug:
                    print "serverlistener", name
                if '*' not in self.apps and name not in self.apps:
                    yield client.rejectConnection(reason='Application not found: ' + name)
                else: # create application instance as needed and add in our list
                    if _debug: print 'name=', name, 'name in apps', str(name in self.apps)
                    app = self.apps[name] if name in self.apps else self.apps['*'] # application class
                    if _debug:
                        print "clients", self.clients.keys()
                    if session in self.clients:
                        inst = self.clients[session][0]
                    else:
                        inst = app()
                    self.clients[session] = [inst]; inst._clients=self.clients[session]
                    msg.server = inst # whew! just in time!
                    try:
                        methodname = "on%s" % msg.command
                        if _debug:
                            print methodname, dir(inst)
                        method = getattr(inst, methodname, None)
                        yield method(client, msg)
                        result = None
                        close_connection = msg.close_connection
                        if _debug:
                            print "close connection", close_connection
                    except:
                        if _debug: traceback.print_exc()
                        yield client.rejectConnection(reason='Exception on %s' % methodname)
                        continue
                    if result is True or result is None:
                        if result is None:
                            msg.wfile.seek(0)
                            data = msg.wfile.read()
                            msg.wfile.seek(0)
                            msg.wfile.truncate()
                            yield client.writeMessage(data)
                            if close_connection:
                                if _debug:
                                    print 'close_connection requested'
                                try:
                                    yield client.connectionClosed()
                                    raise ConnectionClosed
                                except ConnectionClosed:
                                    if _debug:
                                        print 'close_connection done'
                                    pass
                    else:
                        if _debug:
                            print "result", result
                        yield client.rejectConnection(reason='Rejected in onConnect')
        except StopIteration: raise
        except:
            if _debug:
                print 'serverlistener exception', \
                (sys and sys.exc_info() or None)
                traceback.print_exc()


# The main routine to start, run and stop the service
if __name__ == '__main__':
    print "optparse"
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-i', '--host',    dest='host',    default='0.0.0.0', help="listening IP address. Default '0.0.0.0'")
    parser.add_option('-p', '--port',    dest='port',    default=8080, type="int", help='listening port number. Default 8080')
    parser.add_option('-r', '--root',    dest='root',    default='./',       help="document root directory. Default './'")
    parser.add_option('-d', '--verbose', dest='verbose', default=False, action='store_true', help='enable debug trace')
    (options, args) = parser.parse_args()

    _debug = options.verbose
    try:
        if _debug: print time.asctime(), 'Options - %s:%d' % (options.host, options.port)
        agent = HTTPServer()
        agent.root = options.root
        agent.start(options.host, options.port)
        if _debug: print time.asctime(), 'HTTP Server Starts - %s:%d' % (options.host, options.port)
        multitask.run()
    except KeyboardInterrupt:
        print traceback.print_exc()
        pass
    except:
        print "exception"
        print traceback.print_exc()
    if _debug: print time.asctime(), 'HTTP Server Stops'
