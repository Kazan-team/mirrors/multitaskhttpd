import multitask
import httpd
from SimpleJSONRPCServer import SimpleJSONRPCRequestHandler
from httpd import HTTPServer, App, BaseApp

class MyApp(SimpleJSONRPCRequestHandler, BaseApp):

    def __init__(self):
        BaseApp.__init__(self)
        SimpleJSONRPCRequestHandler.__init__(self)

        self.register_function(self.echo)

    def echo(self, parm):
        return parm

httpd.set_debug(True)
agent = HTTPServer()   
agent.apps = dict({'/json': MyApp, '*': App})
agent.start(port=8089)
multitask.run() 

