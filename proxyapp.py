""" This is a proxy server which converts incoming requests into
    HTTP/1.1 with Keep-Alive set.  It maintains a continuous
    HTTP connection to the server it is proxying for, so that
    as long as that remote server keeps a process or thread
    running to serve the continuous stream of requests, that
    process or thread can keep state information in memory.

    For example, a connection object to a database can be kept
    around; large complex data structures that are too big to
    serialise or pass across inter-process boundaries and so on.

    The target proxy HTTP Server *must* support HTTP keep-alives
    (header "Connection: keep-alive").

    The client (browser, usually) *does not* have to support
    HTTP keep-alives, or maintain a continuous connection: that
    action is performed by the proxy.

    However, the only requirements is that the client (browser)
    *must* support and accept cookies, as the cookie named "session"
    is used by multitaskhttpd to identify and route the incoming
    connections through to the correct persistent proxy connection.
"""

import multitask
import httpd
from ProxyServer import ProxyServerRequestHandler
from httpd import HTTPServer, App, BaseApp

class ProxyApp(ProxyServerRequestHandler, BaseApp):

    def __init__(self):
        BaseApp.__init__(self)
        ProxyServerRequestHandler.__init__(self)
        self.proxies = {}

if __name__ == '__main__':
    #httpd.set_debug(True)
    agent = HTTPServer()   
    agent.apps = dict({'*': ProxyApp})
    agent.start()
    multitask.run() 

